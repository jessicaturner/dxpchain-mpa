"""
use this script to publish a feed with hard code
"""
from getpass import getpass
from pricefeed_final import publish_feed

# input prices here must include cny, btc, and usd rate
prices = {"feed": {"DXP:CNY": 0.126912, "DXP:BTC": 0.00000215, "DXP:USD": 0.0185317}}


name = input("\n\nDxpchain DEX account name:\n\n")
wif = getpass("\n\nDxpchain DEX wif:\n\n")
publish_feed(prices, name, wif)
