"""
public api nodes
"""


def public_nodes():
    """
    use these nodes for all operations
    """
    return [
        # "wss://api.dxp.mobi/wss",
        # "wss://api-us.61dxp.com/wss",
        # "wss://cloud.xdxp.io/ws",
        # "wss://api.dex.trading/wss",
        # "wss://eu.nodes.dxpchain.ws/ws",
        # "wss://api.pindd.club/ws",
        # "wss://dex.iobanker.com/ws",
        # "wss://public.xdxp.io/ws",
        # "wss://node.xdxp.io/ws",
        # "wss://node.market.rudex.org/ws",
        # "wss://nexus01.co.uk/ws",
        # "wss://api-dxp.liondani.com/ws",
        # "wss://api.dxpchain.bhuz.info/wss",
        # "wss://dxpws.roelandp.nl/ws",
        # "wss://hongkong.dxpchain.im/ws",
        # "wss://node1.deex.exchange/wss",
        # "wss://api.cnvote.vip:888/wss",
        # "wss://dxp.open.icowallet.net/ws",
        # "wss://api.weaccount.cn/ws",
        # "wss://api.61dxp.com",
        # "wss://api.dxpgo.net/ws",
        # "wss://dxpchain.dxp123.cc:15138/wss",
        # "wss://singapore.dxpchain.im/wss",
        "ws://localhost:11011"
    ]


def universe():
    """
    this is just a place to store other known endpoints; it is NOT USED BY THE SCRIPT
    last latency tested July 2020
    """
    return [
        # "wss://blockzms.xyz/ws",
        # "wss://api.dxp.mobi/ws",
        # "wss://api.dxpchain.bhuz.info",
        # "wss://cloud.xdxp.io/wss",
        # "wss://us-mo.walldex.pro/wss",
        # "wss://node1.deex.exchange/wss",
        # "wss://eu.nodes.dxpchain.ws/ws",
        # "wss://node.dxpchain.eu/wss",
        # "wss://ca-bc.walldex.pro/ws",
        # "wss://public.xdxp.io/ws",
        # "wss://node.market.rudex.org/ws",
        # "wss://node.xdxp.io/wss",
        # "wss://api-dxp.liondani.com/ws",
        # "wss://dex.iobanker.com:9090/ws",
        # "wss://dxpfullnode.bangzi.info/ws",
        # "wss://france.walldex.pro/wss",
        # "wss://dxpws.roelandp.nl/ws",
        # "wss://chile.walldex.pro/ws",
        # "wss://api.gbacenter.org/ws",
        # "wss://australia.walldex.pro/ws",
        # "wss://api.cnvote.vip:888/ws",
        # "wss://india.walldex.pro/wss",
        # "wss://node6.deexnet.org/wss",
        # "wss://dxp.open.icowallet.net/ws",
        # "wss://singapore.walldex.pro/wss",
        # "wss://kimziv.com/ws",
        # "wss://api.weaccount.cn/ws",
        # "wss://api.dxpgo.net/ws",
        # "wss://dxpchain.dxp123.cc:15138/wss",
        # "wss://api.61dxp.com",
        # "wss://freedom.dxp123.cc:15138/wss",
        # "wss://ws.gdex.top/ws",
        "ws://localhost:11011"
    ]
