under construction...


BSIP 16

limits the maximum that can be settled per period on Smarttokens 1.0

https://github.com/dxpchain/bsips/blob/master/bsip-0016.md

BSIP 76 

ensures that Smarttokens 1.0 are no longer market pegged to their respective currencies

BSIP 42 

ensures that Smarttokens 1.0 are not subject to margin calls any deeper than 2% below market price

